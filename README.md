# Build Your Own Kaniko Container

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/containers/kaniko-docker-build)

## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/master/README.md)

## Working Design Pattern

As originally built, this design pattern works and can be tested. In the case of plugin extensions like this one, the working pattern may be it's use in another Guided Exploration.

## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

* **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

* **Publish Date**: 2020-05-15

* **GitLab Version Released On**: 13.0

* **GitLab Edition Required**: 

  * For overall solution: [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/) 

    [Click to see Features by Edition](https://about.gitlab.com/features/) 

- **Tested On**: 
  - GitLab Docker-Executor Runner (GitLab.com Shared Runner).

## References and Featured In

* The custom containers built by this repository are used in a practical example here: https://gitlab.com/guided-explorations/containers/aws-cli-tools

## Why Build Your Own?
When using kaniko in CI on systems that support containers for building (like GitLab CI :) ) - it is handy to be able to run specific commands in the Kaniko container. The simpliest case being running git to retrieve the last tag in the repo (even if not on the same branch).  To accomplish this GitLab CI uses the debug container which is barebones with the busybox shell on it.  Consequently there is no package manager and no way to bring on additional packages.

## Architecture / Constraints

1. Always grab the original Kaniko repo to get the latest version.
2. Due to the testing done in the original repo, just build the Dockerfile without running the full original CI

## Demonstrates These Design Requirements, Desirements and AntiPatterns

- **Kaniko / Docker Development Pattern:** Uses a git container to pass a copy of the repository into DinD or Kaniko so we can dynamically splice changes into the original Google source for Kaniko.
- **Docker Development Pattern:** Build your own Kaniko container and push to internal Gitlab Container Registry or Docker Hub (external registry example).
- **Docker Development Pattern:** Comply with opencontainers.org labeling (https://github.com/opencontainers/image-spec/blob/master/annotations.md#back-compatibility-with-label-schema)
- **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Use Docker in Docker to build containers and push to internal Gitlab Container Registry or Docker Hub (external registry example) discussed here: https://docs.gitlab.com/ee/ci/docker/using_kaniko.html 
- **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Image Labeling with additional GitLab CI build meta data
- **Docker Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Add the git version tag - either with the last available version tag in git or ONLY if the current commit has a version (configurable)
- **Docker Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Adding set of arbitrary tags in space delimited variable (compact and flexible)
- **Docker Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Adding "latest" tag only when building master branch.
- **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Using variable scoping to override specific variables discussed here to push to another repo: https://docs.gitlab.com/ee/ci/variables/#limiting-environment-scopes-of-environment-variables


## Cross References and Documentation

- Kaniko Documentation (to go beyond the basic example in GitLab documentation): https://github.com/GoogleContainerTools/kaniko/blob/master/README.md
